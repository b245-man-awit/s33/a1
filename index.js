// GET method title
fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
    .then(response =>response.json())
    .then(data =>{ 
        let title = data.map(element=>element.title)
        console.log(title);
    });

// GET method -  title and status
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})
    .then(response =>response.json())
    .then(data =>{ 
            let title = data.title,
            completed=  data.completed
            console.log(data)
            console.log(`The item "${data.title}" on the list has a status of ${data.completed}`) 

        }
         
     
    );


// POST method - create
fetch("https://jsonplaceholder.typicode.com/todos"
,{  
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: false,
        id: 201,
        title: "Created to do list",
        userId: 1

    })
})
.then(response => response.json())
.then(result => console.log(result));


// PUT method -
fetch("https://jsonplaceholder.typicode.com/todos/1"
,{  
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Update To Do List",
        description: "To update to do list with a different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId:1
    })
})
.then(response => response.json())
.then(result => console.log(result));

// PATCH 
fetch("https://jsonplaceholder.typicode.com/todos/1",{
        method:"PATCH",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            status: "Completed",
            dateCompleted: "01/31/2023",
        })
    })
    .then(response => response.json())
    .then(result=>console.log(result));


// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method: "DELETE"
})
.then(response => response.json())
.then(result=>console.log(result));    